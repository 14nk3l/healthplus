﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de ContatoBO
/// </summary>
public class ContatoBO
{
    public string Gravar(Contato obj)
    {
        if (obj.Nome == string.Empty)
        {
            return "O campo nome é obrigatório";
        }
        if (obj.Comentario == string.Empty)
        {
            return "O campo matrícula é obrigatório";
        }
        if (obj.Email == string.Empty)
        {
            return "O campo matrícula é obrigatório";
        }

        new ContatoDAO().Gravar(obj);
        return "Mensagem enviada com sucesso!";
    }
    public IList<Contato> Listar()
    {
        return new ContatoDAO().Lista();
    }

    public Contato Buscar(int contatoID)
    {
        return new ContatoDAO().Buscar(contatoID);
    }
}