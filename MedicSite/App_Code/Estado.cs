﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Estado
/// </summary>
public class Estado
{

    public int EstadoID { get; set; }
    public string Descricao { get; set; }
}