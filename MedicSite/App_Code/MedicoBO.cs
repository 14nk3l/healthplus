﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de MedicoBO
/// </summary>
public class MedicoBO
{
    public string Gravar(Medico obj)
    {
        if (obj.Nome == string.Empty)
        {
            return "O campo nome é obrigatório";
        }

        if (obj.Cpf == string.Empty)
        {
            return "O campo  CPF  é obrigatório";
        }
       
        if (obj.DataNascimento.Date.ToString() == "01/01/0001 00:00:00")
        {
            return "O campo data é obrigatório";
        }
        new MedicoDao().Gravar(obj);

        return "Cadastro Salvo com Sucesso!";
    }
    public IList<Medico> Listar()
    {
        return new MedicoDao().Lista();
    }
    public IList<Medico> ListarPorEspecialidade(int especialidadeid)
    {
        return new MedicoDao().ListaPorEspecialidade(especialidadeid);
    }
    public IList<Medico> ListarPorEspecialidadeMunicipio(int especialidadeid, int municipioid)
    {
        return new MedicoDao().ListaPorEspecialidadeMunicipio(especialidadeid,municipioid);
    }
    public Medico Buscar(int parceiroid)
    {
        return new MedicoDao().Buscar(parceiroid);
    }
    
}