﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Municipio
/// </summary>
public class Municipio
{

    public int MunicipioID { get; set; }
    public string Descricao { get; set; }
    public int EstadoID { get; set; }
}