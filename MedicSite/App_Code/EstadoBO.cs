﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de EstadoBO
/// </summary>
public class EstadoBO
{
    public string Gravar(Estado obj)
    {
        if (obj.Descricao == string.Empty)
        {
            return "O campo Descricao é obrigatório";
        }
        
        new EstadoDao().Gravar(obj);

        return "Mensagem enviada com sucesso!";
    }
    public IList<Estado> Listar()
    {
        return new EstadoDao().Lista();
    }

    public Municipio Buscar(int estadoID)
    {
        return new MunicipioDao().Buscar(estadoID);
    }
}