﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Contato
/// </summary>
public class Contato
{
   
        public int ContatoID { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Comentario { get; set; }
    
}