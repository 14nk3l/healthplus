﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Descrição resumida de MunicipioDao
/// </summary>
public class MunicipioDao
{
    Database banco = Database.Open("dbhealtplus");

    public void Gravar(Municipio obj)
    {
        if (obj.MunicipioID == 0)
        {
            var query = "Insert into Municipio(Descricao,EstaoID) " +
                    "Values(@0,@1)";
            banco.Execute(query, obj.Descricao, obj.EstadoID);
            banco.Close();
        }
        else
        {
            var query = "Update Municipio set Descricao=@0, EstadoID=@1 where MunicipioID=@3";
            banco.Execute(query, obj.Descricao, obj.EstadoID,
                obj.MunicipioID);
            banco.Close();

        }

    }
    public IList<Municipio> Lista()
    {
        IList<Municipio> listaMunicipio = new List<Municipio>();
        var query = "Select * From Municipio";
        var resultado = banco.Query(query);
        if (resultado.Count() > 0)
        {
            Municipio objMunicipio;
            foreach (var item in resultado)
            {
                objMunicipio = new Municipio
                {
                    MunicipioID = item.MunicipioID,
                    Descricao = item.Descricao,
                    EstadoID = item.EstadoID
                };
                listaMunicipio.Add(objMunicipio);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaMunicipio;
    }


    public IList<Municipio> ListaPorEstado(int estadoid)
    {
        IList<Municipio> listaMunicipio = new List<Municipio>();
        var query = "Select * From Municipio where EstadoID=@0";
        var resultado = banco.Query(query,estadoid);
        if (resultado.Count() > 0)
        {
            Municipio objMunicipio;
            foreach (var item in resultado)
            {
                objMunicipio = new Municipio
                {
                    MunicipioID = item.MunicipioID,
                    Descricao = item.Descricao,
                    EstadoID = item.EstadoID
                };
                listaMunicipio.Add(objMunicipio);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaMunicipio;
    }



    public Municipio Buscar(int municipioID)
    {
        var query = "Select * From Municipio Where MunicipioID = @0";
        var resultado = banco.QuerySingle(query, municipioID);
        Municipio objMunicipio = new Municipio
        {
            MunicipioID = resultado.MunicipioID,
            Descricao = resultado.Descricao,
            EstadoID = resultado.EstadoID
         
        };
        banco.Close();
        return objMunicipio;
    }

}