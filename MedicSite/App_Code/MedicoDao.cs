﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;



/// <summary>
/// Descrição resumida de MedicoDao
/// </summary>
public class MedicoDao
{
    Database banco = Database.Open("dbHealtplus");

    public void Gravar(Medico obj)
    {


        if (obj.ParceiroID == 0)
        {
            var query = "Insert into Parceiro(nome,Cpf," +
                    "DataNascimento,CRM,Sexo,EspecialidadeID, Competencias,Telefone, Email,Endereco, Cep,EnderecoNumero,MunicipioID,Foto) " +
                    "Values(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11, @12, @13)";
            banco.Execute(query, obj.Nome, obj.Cpf,
                obj.DataNascimento, obj.CRM, obj.Sexo, obj.EspecialidadeID,obj.Competencias, obj.Telefone,
                obj.Email,  obj.Endereco, obj.Cep,obj.EnderecoNumero, obj.MunicipioID, obj.Foto);
            banco.Close();
        }
        else
        {
            var query = "Update Parceiro Set nome=@0,Cpf=@1," +
                    "DataNascimento=@2,CRM=@3,Sexo=@4,EspecialidadeID=@5, Competencias=@6,Telefone=@7, Email=@8,Endereco=@9,Cep=@10,EnderecoNumero=@11,MunicipioID=@12,Foto=@13 Where MedicoID=@14";
            banco.Execute(query, obj.Nome, obj.Cpf,
                obj.DataNascimento, obj.CRM, obj.Sexo, obj.EspecialidadeID,obj.Competencias,obj.Telefone, obj.Email, obj.Endereco, obj.Cep, obj.EnderecoNumero, obj.MunicipioID, obj.Foto, obj.ParceiroID);
            banco.Close();

        }

    }


    public IList<Medico> Lista()
    {
        IList<Medico> listaMedico = new List<Medico>();
        var query = "Select * From Parceiro";
        var resultado = banco.Query(query);
        if (resultado.Count() > 0)
        {
            Medico objMedico;
            foreach (var item in resultado)
            {
                objMedico = new Medico
                {
                    ParceiroID = item.ParceiroID,
                    Nome = item.nome,
                    Cpf = item.cpf,
                    DataNascimento = item.dataNascimento,
                    CRM = item.CRM,
                    Sexo = char.Parse(item.sexo),
                    EspecialidadeID = item.EspecialidadeID,
                    Competencias = item.Competencias,
                    Telefone = item.Telefone,
                    Email = item.Email,
                    Endereco = item.Endereco,
                    Cep = item. Cep,
                    EnderecoNumero = item.EnderecoNumero,
                    MunicipioID = item.MunicipioID,
                    Foto = item.Foto,



                };
                listaMedico.Add(objMedico);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaMedico;
    }


    public Medico Buscar(int parceiroid)
    {
        var query = "Select * From Parceiro Where ParceiroID = @0";
        var resultado = banco.QuerySingle(query, parceiroid);
        Medico ObjMedico = new Medico
        {
            ParceiroID = resultado.ParceiroID,
            Nome = resultado.Nome,
            Cpf = resultado.Cpf,
            DataNascimento = resultado.dataNascimento,
            CRM = resultado.CRM,
            Sexo = Char.Parse(resultado.sexo),
            EspecialidadeID = resultado.EspecialidadeID,
            Competencias = resultado.Competencias,
            Telefone = resultado.Telefone,
            Email = resultado.Email,
            Endereco = resultado.Endereco,
            Foto = resultado.Foto,

        };
        banco.Close();
        return ObjMedico;
    }

   
    



    public IList<Medico> ListaPorEspecialidade( int EspecialidadeID)
    {
        IList<Medico> listaMedico = new List<Medico>();
        var query = "Select * From Parceiro where EspecialidadeID="+EspecialidadeID;
        var resultado = banco.Query(query);
        if (resultado.Count() > 0)
        {
            Medico objMedico;
            foreach (var item in resultado)
            {
                objMedico = new Medico
                {
                    ParceiroID = item.ParceiroID,
                    Nome = item.nome,
                    Cpf = item.cpf,
                    DataNascimento = item.dataNascimento,
                    CRM = item.CRM,
                    Sexo = char.Parse(item.sexo),
                    EspecialidadeID = item.EspecialidadeID,
                    Competencias = item.Competencias,
                    Telefone = item.Telefone,
                    Email = item.Email,
                    Endereco = item.Endereco,
                    Cep = item.Cep,
                    EnderecoNumero = item.EnderecoNumero,
                    MunicipioID = item.MunicipioID,
                    Foto = item.Foto,



                };
                listaMedico.Add(objMedico);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaMedico;
    }

    public IList<Medico> ListaPorEspecialidadeMunicipio(int EspecialidadeID, int MunicipioID)
    {
        IList<Medico> listaMedico = new List<Medico>();
        var query = "Select * From Parceiro where EspecialidadeID=@0 and MunicipioID=@2";
        var resultado = banco.Query(query);
        if (resultado.Count() > 0)
        {
            Medico objMedico;
            foreach (var item in resultado)
            {
                objMedico = new Medico
                {
                    ParceiroID = item.ParceiroID,
                    Nome = item.nome,
                    Cpf = item.cpf,
                    DataNascimento = item.dataNascimento,
                    CRM = item.CRM,
                    Sexo = char.Parse(item.sexo),
                    EspecialidadeID = item.EspecialidadeID,
                    Competencias = item.Competencias,
                    Telefone = item.Telefone,
                    Email = item.Email,
                    Endereco = item.Endereco,
                    Cep = item.Cep,
                    EnderecoNumero = item.EnderecoNumero,
                    MunicipioID = item.MunicipioID,
                    Foto = item.Foto,



                };
                listaMedico.Add(objMedico);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaMedico;
    }

}