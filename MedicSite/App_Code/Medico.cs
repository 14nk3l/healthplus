﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Medico
/// </summary>
public class Medico
{
    public int ParceiroID { get; set; }
    public string Nome { get; set; }   
    public string Cpf { get; set; }
    public DateTime DataNascimento { get; set; }
    public string CRM { get; set; }
    public char Sexo { get; set; }
    public int EspecialidadeID { get; set; }
    public string Competencias { get; set; }
    public string Telefone { get; set; }
    public string Email { get; set; }
    public string Endereco { get; set; }
    public string Cep { get; set; }
    public int EnderecoNumero { get; set; }
    public int MunicipioID { get; set; }
    public string Foto { get; set; }




}