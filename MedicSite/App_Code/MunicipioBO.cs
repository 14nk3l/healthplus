﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de MunicipioBO
/// </summary>
public class MunicipioBO
{
    public string Gravar(Municipio obj)
    {
        if (obj.Descricao == string.Empty)
        {
            return "O campo Descricao é obrigatório";
        }
        if (obj.EstadoID == 0)
        {
            return "Selecione o Estado ";
        }
        

        new MunicipioDao().Gravar(obj);

        return "Mensagem enviada com sucesso!";
    }
    public IList<Municipio> Listar()
    {
        return new MunicipioDao().Lista();
    }

    public IList<Municipio> ListarporEstado(int estadoid)
    {
        return new MunicipioDao().ListaPorEstado(estadoid);
    }
    public Municipio Buscar(int municipioID)
    {
        return new MunicipioDao().Buscar(municipioID);
    }
}