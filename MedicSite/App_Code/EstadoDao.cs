﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Descrição resumida de EstadoDao
/// </summary>
public class EstadoDao
{
    Database banco = Database.Open("dbhealtplus");

    public void Gravar(Estado obj)
    {
        if (obj.EstadoID == 0)
        {
            var query = "Insert into Estado(Descricao) " +
                    "Values(@0)";
            banco.Execute(query, obj.Descricao);
            banco.Close();
        }
        else
        {
            var query = "Update Estado set Descricao=@0 where EstadoID=@1";
            banco.Execute(query, obj.Descricao,obj.EstadoID);
            banco.Close();

        }

    }
    public IList<Estado> Lista()
    {
        IList<Estado> listaEstado = new List<Estado>();
        var query = "Select * From Estado";
        var resultado = banco.Query(query);
        if (resultado.Count() > 0)
        {
            Estado objEstado;
            foreach (var item in resultado)
            {
                objEstado = new Estado
                {
                    EstadoID = item.EstadoID,
                    Descricao = item.Descricao,
                                   };
                listaEstado.Add(objEstado);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaEstado;
    }
    public Estado Buscar(int EstadoID)
    {
        var query = "Select * From Estado Where EstadoID = @0";
        var resultado = banco.QuerySingle(query, EstadoID);
        Estado objEstado = new Estado
        {
            EstadoID = resultado.EstadoID,
            Descricao = resultado.Descricao,
           

        };
        banco.Close();
        return objEstado;
    }
}