﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Especialidade
/// </summary>
public class Especialidade
{

    public int EspecialidadeID { get; set; }
    public string Descricao { get; set; }
}