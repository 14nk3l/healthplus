﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de EspecialidadeBO
/// </summary>
public class EspecialidadeBO
{
    public string Gravar( Especialidade obj)
    {
        if(obj.Descricao==string.Empty)
        {
            return (" Informe a Descriçao");
        }

        new EspecialidadeDao().Gravar(obj);
        return ("Gravado com sucesso");
    }

    public IList<Especialidade> Listar()
    { 
        
        return new EspecialidadeDao().Lista();
    }

    public Especialidade Buscar(int especialidadeid)
    {
        return new EspecialidadeDao().Buscar(especialidadeid);

    }

}