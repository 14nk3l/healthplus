﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Descrição resumida de EspecialidadeDao
/// </summary>
public class EspecialidadeDao
{
    Database banco = Database.Open("dbHealtplus");

    public void Gravar(Especialidade obj)
    {
        if (obj.EspecialidadeID == 0)
        {
            var query = "Insert into Especialidade(Descricao) " +
                    "Values(@0)";
            banco.Execute(query, obj.Descricao);
            banco.Close();
        }
        else
        {
            var query = "Update Especialidade Set Descricao=@0 Where EspecialidadeID=@1";
            banco.Execute(query, obj.Descricao,obj.EspecialidadeID);
            banco.Close();

        }

    }
    public IList<Especialidade> Lista()
    {
        IList<Especialidade> listaEspecialidade = new List<Especialidade>();
        var query = "Select * From Especialidade";
        var resultado = banco.Query(query);
        if (resultado.Count() > 0)
        {
            Especialidade objEspecialidade;
            foreach (var item in resultado)
            {
                objEspecialidade = new Especialidade
                {
                    EspecialidadeID = item.EspecialidadeID,
                    Descricao = item.Descricao
                  
                };
                listaEspecialidade.Add(objEspecialidade);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaEspecialidade;
    }
    public Especialidade Buscar(int especialidadeid)
    {
        var query = "Select * From Especialidade Where EspecialidadeID = @0";
        var resultado = banco.QuerySingle(query, especialidadeid);
        Especialidade objEspecialidade = new Especialidade
        {
            EspecialidadeID = resultado.EspecialidadeID,
            Descricao = resultado.Descricao,
            
        };
        banco.Close();
        return objEspecialidade;
    }

}