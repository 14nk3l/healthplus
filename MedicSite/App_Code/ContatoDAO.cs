﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

/// <summary>
/// Descrição resumida de ContatoDAO
/// </summary>
public class ContatoDAO
{
    Database banco = Database.Open("dbhealtplus");

    public void Gravar(Contato obj)
    {
        if (obj.ContatoID == 0)
        {
            var query = "Insert into contatos(nome,email," +
                    "comentario) " +
                    "Values(@0,@1,@2)";
            banco.Execute(query, obj.Nome, obj.Email, obj.Comentario);
            banco.Close();
        }
        else
        {
            var query = "Update contatos set nome=@0," +
                "email=@1,comentario=@2 where contatoID=@3";
            banco.Execute(query, obj.Nome, obj.Email,
                obj.Comentario, obj.ContatoID);
            banco.Close();

        }

    }
    public IList<Contato> Lista()
    {
        IList<Contato> listaContato = new List<Contato>();
        var query = "Select * From contatos";
        var resultado = banco.Query(query);
        if (resultado.Count() > 0)
        {
            Contato objContato;
            foreach (var item in resultado)
            {
                objContato = new Contato
                {
                    ContatoID = item.contatoID,
                    Nome = item.nome,
                    Email = item.email,
                    Comentario = item.comentario,
                };
                listaContato.Add(objContato);
            }
            banco.Close();
        }
        else
        {
            banco.Close();
            return null;
        }
        return listaContato;
    }
    public Contato Buscar(int contatoID)
    {
        var query = "Select * From contatos Where contatoID = @0";
        var resultado = banco.QuerySingle(query, contatoID);
        Contato objContato = new Contato
        {
            ContatoID = resultado.contatoID,
            Nome = resultado.nome,
            Email = resultado.email,
            Comentario = resultado.comentario
        };
        banco.Close();
        return objContato;
    }


}